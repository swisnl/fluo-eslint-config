# @swis/eslint-config-fluo

> The default eslint config for Fluo projects.

Extends [`eslint:recommended`](https://github.com/eslint/eslint/blob/main/conf/eslint-recommended.js).
Based upon drupal/core's[`.eslintrc.legacy.json`](https://github.com/eslint/eslint/blob/main/conf/eslint-recommended.js).

# Howto

Run: `npm install @swis/eslint-config-fluo`

In your `.eslintrc`:
```
{
    "extends": "@swis/eslint-config-fluo"
}
```